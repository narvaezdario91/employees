package com.thalesgroup.employeesBackend.controllers;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thalesgroup.employeesBackend.dummy.Employee;
import com.thalesgroup.employeesBackend.services.EmployeeClientRestService;


@CrossOrigin
@RestController
@RequestMapping("/api/v1/employees")
public class EmployeeController {
	
	@Autowired
	EmployeeClientRestService employeeClientRestService;
	
	@GetMapping()
	public ArrayList<Employee> getEmployees(){
		return (ArrayList<Employee>) employeeClientRestService.getEmployees();
	}
	
	@GetMapping(path  = "/{id}")
	public ArrayList<Employee> getEmployeeById(@PathVariable("id") Long id){
		return (ArrayList<Employee>) employeeClientRestService.getEmployeeById(id);
	}
	
}
