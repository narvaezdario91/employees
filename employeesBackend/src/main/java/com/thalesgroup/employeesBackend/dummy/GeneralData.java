package com.thalesgroup.employeesBackend.dummy;

import java.util.ArrayList;
import java.util.List;

public class GeneralData implements GeneralDataInterface{
	private String status;
	private String message;
	private Employee data;
	
	
	@Override
	public String getStatus() {
		return status;
	}
	
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public List<Employee> getData() {
		List<Employee> listEmployee = new ArrayList<>();
		listEmployee.add(data);
		return listEmployee;
	}
	
	
	public void setEmployee(Employee data) {
		this.data= data;
	}
	
	
}
