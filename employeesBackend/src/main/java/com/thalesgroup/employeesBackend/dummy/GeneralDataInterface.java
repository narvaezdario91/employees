package com.thalesgroup.employeesBackend.dummy;

import java.util.List;

public interface GeneralDataInterface {
	public String getStatus();
	public String getMessage();
	public List<Employee> getData();
}
