package com.thalesgroup.employeesBackend.dummy;

import java.util.ArrayList;
import java.util.List;

public class GeneralDataList implements GeneralDataInterface{
	private String status;
	private String message;
	private List<Employee> data;
	
	
	public GeneralDataList() {
        data = new ArrayList<>();
    }

	@Override
	public String getStatus() {
		return status;
	}

	
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String getMessage() {
		return message;
	}

	
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public List<Employee> getData() {
		return data;
	}

	
	public void setData(List<Employee> data) {
		this.data = data;
	}
	
	
	
}
