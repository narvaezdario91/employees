package com.thalesgroup.employeesBackend.dummy;

import java.util.ArrayList;
import java.util.List;

public class NullGeneralData implements GeneralDataInterface {

	private List<Employee> data;
	private String message;
	private String status;

	
    private static NullGeneralData nullGeneralData;
 
 
	public static NullGeneralData getNullGeneralData() {
		if (nullGeneralData == null) {
			nullGeneralData = new NullGeneralData();
		}
		return nullGeneralData;
	}


	private NullGeneralData() {
		this.status = "200";
		this.message = "Empty List";
		this.data = new ArrayList<>();
	}

	@Override
	public String getStatus() {
		return this.status;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	@Override
	public List<Employee> getData() {
		return this.data;
	}

}
