package com.thalesgroup.employeesBackend.services;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.thalesgroup.employeesBackend.dummy.*;

@Service
public class EmployeeClientRestService {
	private final RestTemplate restTemplate;
	private final String URL_EMPLOYEES = "http://dummy.restapiexample.com/api/v1/employees";
	private final String URL_EMPLOYEE = "http://dummy.restapiexample.com/api/v1/employee/";

	Logger logger = LoggerFactory.getLogger(EmployeeClientRestService.class);

	public EmployeeClientRestService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	public List<Employee> getEmployees() {
		List<Employee> employees = NullGeneralData.getNullGeneralData().getData();
		try {
			GeneralDataList response = restTemplate.getForObject(URL_EMPLOYEES, GeneralDataList.class);
			if(!response.getData().contains(null))
				employees = response.getData();
		} catch (HttpClientErrorException exception) {
			logger.info("Many Request on the Server Dummy");
		}

		return employees;
	}

	public List<Employee> getEmployeeById(Long id) {
		List<Employee> employees = NullGeneralData.getNullGeneralData().getData();
		try {
			GeneralData response = this.restTemplate.getForObject(URL_EMPLOYEE + id, GeneralData.class);
			if(!response.getData().contains(null))
				employees = response.getData();

		} catch (HttpClientErrorException exception) {
			logger.info("Many Request on the Server Dummy");
		}

		return employees;
	}
}
