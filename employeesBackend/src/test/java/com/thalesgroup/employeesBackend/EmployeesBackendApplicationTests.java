package com.thalesgroup.employeesBackend;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.thalesgroup.employeesBackend.controllers.EmployeeController;
import com.thalesgroup.employeesBackend.dummy.Employee;
import com.thalesgroup.employeesBackend.dummy.GeneralDataList;
import com.thalesgroup.employeesBackend.services.EmployeeClientRestService;

@ExtendWith(MockitoExtension.class)
class EmployeesBackendApplicationTests {

	@Mock
	private EmployeeClientRestService employeeService;

    @InjectMocks
    private EmployeeController employeeController;
    

	@Test
	public void testGetEmployees() {
		
		List<Employee> listEmployees = new ArrayList<>();
		Employee empOne = new Employee();
		empOne.setEmployee_name("Dario");
		Employee empTwo = new Employee();
		empOne.setEmployee_name("Leonardo");
		Employee empThree = new Employee();
		empOne.setEmployee_name("Leonardo");
		
		listEmployees.add(empOne);
		listEmployees.add(empTwo);
		listEmployees.add(empThree);
		
		when(employeeService.getEmployees()).thenReturn(listEmployees);
		
		assertFalse(employeeController.getEmployees().isEmpty());
	}
	
	@Test
	public void testGetEmployee() {
		List<Employee> listEmployees = new ArrayList<>();
		Employee empOne = new Employee();
		empOne.setEmployee_name("Dario");
		empOne.setId(1L);
		
		listEmployees.add(empOne);
	
		when(employeeService.getEmployeeById(1L)).thenReturn(listEmployees);	
		assertEquals(1, employeeController.getEmployeeById(1L).size());
	}

}
