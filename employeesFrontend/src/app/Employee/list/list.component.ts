import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from 'src/app/Model/Employee';
import { EmployeeServiceService } from 'src/app/Service/employee-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  employees:Employee[];
  employeeID: string;

  constructor(private employeeService:EmployeeServiceService, private router: Router) { 

  }

  ngOnInit(): void {
    this.employeeService.getEmployees()
      .subscribe(data=>{
        this.employees=data;
      });
  }

  searchEmployees(){
    if(this.employeeID){
      this.employeeService.getEmployee(this.employeeID)
      .subscribe(data=>{
        this.employees=data;
      });
    }else{
      this.employeeService.getEmployees()
      .subscribe(data=>{
        this.employees=data;
      });
    }
    
  }

}
