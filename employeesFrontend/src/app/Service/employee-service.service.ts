import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Employee } from '../Model/Employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  constructor(private http:HttpClient) { }

  URL = 'https://employees-thales.herokuapp.com/api/v1/employees'

  getEmployees(){
    return this.http.get<Employee[]>(this.URL);
  }

  getEmployee(id){
    return this.http.get<Employee[]>(this.URL+'/'+id);
  }
}
